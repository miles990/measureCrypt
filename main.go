package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/miles990/measureCrypt/crypt/aes"
	"gitlab.com/miles990/measureCrypt/crypt/ecc"
	"gitlab.com/miles990/measureCrypt/data"
	"gitlab.com/miles990/measureCrypt/models"
	"gitlab.com/miles990/measureCrypt/pb"

	"github.com/ethereum/go-ethereum/crypto/ecies"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

var (
	AesKey     = []byte("example key 1234")
	PrivateKey *ecies.PrivateKey
	PublicKey  *ecies.PublicKey
)

var (
	jsonBytes []byte
	pbBytes   []byte
)

func JsonAesDemo(key []byte, encryptData []byte) {

	// defer timeTrack(time.Now(), "json aes")
	encryptedJsonData, err := aes.Encrypt(encryptData, key)
	// fmt.Printf("AES encrypted json data: %x\n", encryptedJsonData)

	decryptedData, err := aes.Decrypt(encryptedJsonData, key)
	if err != nil {
		fmt.Println(err)
		return
	}

	payloadData := &models.Payload{}
	err = json.Unmarshal(decryptedData, &payloadData)
	if err != nil {
		panic(err)
	}
	// fmt.Printf("AES encrypted json data size: %d\n", len(encryptedJsonData))
}

func ProtobufAesDemo(key []byte, encryptData []byte) {
	// defer timeTrack(time.Now(), "protobuf aes")

	encryptedProtobufData, err := aes.Encrypt(encryptData, AesKey)
	// fmt.Printf("AES encrypted protobuf data: %x\n", encryptedProtobufData)

	decryptedData, err := aes.Decrypt(encryptedProtobufData, AesKey)
	if err != nil {
		fmt.Println(err)
		return
	}
	pbData := &pb.Payload{}
	err = proto.Unmarshal(decryptedData, pbData)
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Printf("AES encrypted protobuf data: %s\n", pbData.String())
	// fmt.Printf("AES encrypted protobuf data size: %d\n", len(encryptedProtobufData))
}

func JsonEccDemo(privateKey *ecies.PrivateKey, publicKey *ecies.PublicKey, encryptData []byte) {
	// defer timeTrack(time.Now(), "json ecc")

	encryptedJsonData, err := ecc.Encrypt(encryptData, *publicKey)
	// fmt.Printf("ECC encrypted json data: %x\n", encryptedJsonData)

	decryptedData, err := ecc.Decrypt(encryptedJsonData, *privateKey)
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Printf("ECC encrypted json data: %s\n", decryptedData)
	// fmt.Printf("ECC encrypted json data size: %d\n", len(encryptedJsonData))
	payloadData := &models.Payload{}
	err = json.Unmarshal(decryptedData, &payloadData)
	if err != nil {
		panic(err)
	}
}

func ProtobufEccDemo(privateKey *ecies.PrivateKey, publicKey *ecies.PublicKey, encryptData []byte) {
	// defer timeTrack(time.Now(), "protobuf ecc")

	encryptedJsonData, err := ecc.Encrypt(encryptData, *publicKey)
	// fmt.Printf("ECC encrypted protobuf data: %x\n", encryptedJsonData)

	decryptedData, err := ecc.Decrypt(encryptedJsonData, *privateKey)
	if err != nil {
		fmt.Println(err)
		return
	}

	pbData := &pb.Payload{}
	err = proto.Unmarshal(decryptedData, pbData)
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Printf("ECC encrypted protobuf data: %s\n", pbData.String())
	// fmt.Printf("ECC encrypted protobuf data size: %d\n", len(encryptedJsonData))
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
	// return fmt.Sprintf("%s", elapsed)
}

func generateKeyFiles() {
	//获取随机字符串
	randomKey := ecc.GetRandomString(40)

	//生成私钥和公钥
	err := ecc.GenerateKey(randomKey)
	if err != nil {
		fmt.Println(err)
	}
}

func loadKeyFiles() {
	var err error
	PrivateKey, err = ecc.GetPrivateKeyByPemFile(ecc.PRIVATEFILE)
	if err != nil {
		fmt.Println(err)
	}

	PublicKey, err = ecc.GetPublicKeyByPemFile(ecc.PUBLICFILE)
	if err != nil {
		fmt.Println(err)
	}
}

func generateTestData() {
	jsonBytes = data.PayloadJsonRaw
	var payload models.Payload
	err := json.Unmarshal(jsonBytes, &payload)
	if err != nil {
		panic(err)
	}

	pbData := &pb.Payload{}
	if err := jsonpb.UnmarshalString(string(jsonBytes), pbData); err != nil {
		panic(err)
	}

	// PrivateKey, PublicKey, err = ecc.GenerateECIESKey()
	// if err != nil {
	// 	panic(err)
	// }

	// protobuf data
	pbBytes, err = proto.Marshal(pbData)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func setup() {
	generateKeyFiles()
	loadKeyFiles()

	generateTestData()
}

func writeCSV(name string, data [][]string) {
	newpath := filepath.Join(".", "csv")
	err := os.MkdirAll(newpath, os.ModePerm)
	if err != nil {
		log.Fatal("MkdirAll", err)
	}
	file, err := os.Create(fmt.Sprintf("csv/%s.csv", name))
	if err != nil {
		log.Fatal("Could not create file", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	header := [][]string{{"costtime"}}
	data = append(header, data...)
	// data := [][]string{
	// 	{"FirstName", "LastName", "Username"},
	// 	{"John", "Doe", "johndoe"},
	// 	{"Jane", "Doe", "janedoe"},
	// }

	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			log.Fatal("Could not write data", err)
		}
	}
}

func MeasureJsonAes(runCount int) {
	data := [][]string{}
	for i := 0; i < runCount; i++ {
		start := time.Now()

		JsonAesDemo(AesKey, jsonBytes)

		elapsed := time.Since(start)
		data = append(data, []string{fmt.Sprintf("%s", elapsed)})
	}
	writeCSV("json_aes", data)
}

func MeasureProtobufAes(runCount int) {
	data := [][]string{}
	for i := 0; i < runCount; i++ {
		start := time.Now()

		ProtobufAesDemo(AesKey, pbBytes)

		elapsed := time.Since(start)
		data = append(data, []string{fmt.Sprintf("%s", elapsed)})
	}
	writeCSV("protobuf_aes", data)
}

func MeasureJsonEcc(runCount int) {
	data := [][]string{}
	for i := 0; i < runCount; i++ {
		start := time.Now()

		JsonEccDemo(PrivateKey, PublicKey, jsonBytes)

		elapsed := time.Since(start)
		data = append(data, []string{fmt.Sprintf("%s", elapsed)})
	}
	writeCSV("json_ecc", data)
}

func MeasureProtobufEcc(runCount int) {
	data := [][]string{}
	for i := 0; i < runCount; i++ {
		start := time.Now()

		ProtobufEccDemo(PrivateKey, PublicKey, pbBytes)

		elapsed := time.Since(start)
		data = append(data, []string{fmt.Sprintf("%s", elapsed)})
	}
	writeCSV("protobuf_ecc", data)
}

func main() {
	testCount := 1000
	setup()
	// 對稱加密
	MeasureJsonAes(testCount)
	MeasureProtobufAes(testCount)

	// 非對稱加密
	MeasureJsonEcc(testCount)
	MeasureProtobufEcc(testCount)
}
