module gitlab.com/miles990/measureCrypt

go 1.20

require (
	github.com/ethereum/go-ethereum v1.12.0
	github.com/golang/protobuf v1.5.3
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.2.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/holiman/uint256 v1.2.2-0.20230321075855-87b91420868c // indirect
	golang.org/x/crypto v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)
