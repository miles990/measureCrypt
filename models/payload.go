package models

type Payload struct {
	Topic string `json:"topic"`
	Data  struct {
		BetInfo []struct {
			BetID    int `json:"betid"`
			TotalBet int `json:"totalbet"`
		} `json:"betinfo"`
		BetTime int `json:"bettime"`
	} `json:"data"`
}
