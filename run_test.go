package main

import (
	"testing"
)

func init() {
	setup()
}

func BenchmarkJsonAes(b *testing.B) {
	// b.StopTimer()
	// setup()
	// b.StartTimer()
	for i := 0; i < b.N; i++ {
		JsonAesDemo(AesKey, jsonBytes)
	}
	// printInfo()
}

func BenchmarkProtobufAes(b *testing.B) {
	// b.StopTimer()
	// setup()
	// b.StartTimer()
	for i := 0; i < b.N; i++ {
		ProtobufAesDemo(AesKey, pbBytes)
	}
}

func BenchmarkJsonEcc(b *testing.B) {
	// b.StopTimer()
	// setup()
	// b.StartTimer()
	for i := 0; i < b.N; i++ {
		JsonEccDemo(PrivateKey, PublicKey, jsonBytes)
	}
}

func BenchmarkProtobufEcc(b *testing.B) {
	// b.StopTimer()
	// setup()
	// b.StartTimer()
	for i := 0; i < b.N; i++ {
		ProtobufEccDemo(PrivateKey, PublicKey, pbBytes)
	}
}
