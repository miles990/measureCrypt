# 測量加解密

##  對稱加密 AES
- func JsonAesDemo()
- func ProtobufAesDemo()

##  非對稱加密 ECC
- func JsonEeeDemo()
- func ProtobufEccDemo()

## 測試流程
1. 產生加解密所需的key
2. 產生測試資料
3. 輸入資料加密（計算花費時間開始）
4. 解密資料
5. 反序列化解密資料（計算花費時間結束）

## 測試結果

```
go test -bench=.
```
![benchmark test](benchmark.png)

## 跑1000次花費時間輸出csv
- [csv file](https://gitlab.com/miles990/measureCrypt/-/tree/main/csv)